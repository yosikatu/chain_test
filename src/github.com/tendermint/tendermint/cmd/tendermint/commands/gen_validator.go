package commands

import (
	"encoding/json"
	"fmt"
	"os"
	"strconv"

	"github.com/spf13/cobra"

	"github.com/tendermint/tendermint/types"
)

// GenValidatorCmd allows the generation of a keypair for a
// validator.
var GenValidatorCmd = &cobra.Command{
	Use:   "gen_validator",
	Short: "Generate new validator keypair",
	Run:   genValidator,
}

func genValidator(cmd *cobra.Command, args []string) {
	privValidator := types.GenPrivValidatorFS("")
	privValidatorJSONBytes, err := json.MarshalIndent(privValidator, "", "\t")
	if err != nil {
		panic(err)
	}
	fmt.Printf(`%v
`, string(privValidatorJSONBytes))
}

// GenValidatorsCmd allows the generation of multi keypairs for validators and combine into one genesis.json
var GenValidatorsCmd = &cobra.Command{
	Use:   "gen_validators",
	Short: "Generate new validators keypair",
	Run:   genValidators,
}

func genValidators(cmd *cobra.Command, args []string) {
	if len(args) > 1 {
		num, err := strconv.Atoi(args[0])
		i := 0
		if err != nil {
			panic(err)
			return
		}

		if _, err := os.Stat(args[1]); os.IsExist(err) {
			os.RemoveAll(args[1])
		}

		genFile := args[1] + "//" + "genesis.json"

		os.Mkdir(args[1], os.ModePerm)

		//if _, err := os.Stat(genFile); os.IsNotExist(err) {
		genDoc := types.GenesisDoc{
			ChainID: "testnet",
		}
		for i < num {
			adr := args[1] + "//node" + strconv.Itoa(i)
			os.Mkdir(adr, os.ModePerm)
			privValidator := types.GenPrivValidatorFS(adr + "//priv_validator.json")
			privValidator.Save()

			genDoc.Validators = append(genDoc.Validators, types.GenesisValidator{
				PubKey: privValidator.GetPubKey(),
				Power:  10,
				Name:   strconv.Itoa(i),
			})
			i++
		}

		if err := genDoc.SaveAs(genFile); err != nil {
			panic(err)
		}
		//}
	} else {
		fmt.Println("Input the count of validators you want to make and the path you want to output.")
		fmt.Println("tendermint gen_validators 10 ~/testnet")
	}
}
